<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>問題画面</title>
<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
	</div><c:forEach items="${ testItems }" var="testItem" >
		<h1>　　　${testItem.testName}</h1>
	<div class="contents">
	<form:form modelAttribute="testForm" id="testForm" action="/eLearning/test/${testItem.testTable}" method="post">
		<ol>
		<c:forEach items="${tests}" var="test">
			<p>
				<li class="question"><h3><c:out value="${test.question}"></c:out></h3></li>
				<input type="radio" name="${test.id}" value="1" required>①${test.answer1}<br>
				<input type="radio" name="${test.id}" value="2">②${test.answer2}<br>
				<input type="radio" name="${test.id}" value="3">③${test.answer3}<br>
				<input type="radio" name="${test.id}" value="4">④${test.answer4}<br>
			<p>
		</c:forEach>
		</ol>
		<form:input type="hidden" path="id" value="${loginUser.id}"/>
		<div class="button"><input class="btn-square-pop2" type="submit" value="回答"></div>
	</form:form>
	<br>
	</div>
	</c:forEach>
</body>
</html>
