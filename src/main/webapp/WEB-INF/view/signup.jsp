<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規登録</title>
<link href="<c:url value="/resources/css/signup.css" />" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<a href="${pageContext.request.contextPath}/management/" class="btn-square-pop">ユーザー管理</a>
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>
	</div>
	<h1>　　　ユーザー新規登録</h1>
	<div id="contents">
		<form:form modelAttribute="UserForm">
			アカウント名（社員番号）
				<div class="cp_iptxt">
					<form:input type="text" path="account" pattern=".*\S+.*" required="required" /><br/>
				</div>
			パスワード
				<div class="cp_iptxt">
					<form:input type="password" path="password" pattern=".*\S+.*" placeholder="半角英数記号（6文字以上20文字以下）" required="required" /><br/>
				</div>
			パスワード(確認用)
				<div class="cp_iptxt">
					<form:input type="password" path="confirmedPassword" pattern=".*\S+.*" placeholder="半角英数記号（6文字以上20文字以下）" required="required"/><br />
					<div class="checkbox"><input type="checkbox" id="password-check"><span>パスワードを表示する</span></div>
					<script type="text/javascript">
						const pwd = document.getElementById('password','confirmedPassword');
						const pwdCheck = document.getElementById('password-check');
						pwdCheck.addEventListener('change', function() {
							if (pwdCheck.checked) {
								pwd.setAttribute('type', 'text');
							} else {
								pwd.setAttribute('type', 'password');
							}
						}, false);
					</script>
				</div>
			名前
				<div class="cp_iptxt">
					<form:input type="text" path="name" pattern=".*\S+.*" required="required"/><br/>
				</div>
			役職<br/>
				<c:forEach items="${managements}" var="management">
					<form:radiobutton path="managementId" value="${management.id}" required="required"></form:radiobutton>${management.name}&nbsp;&nbsp;&nbsp;
				</c:forEach>
			<br>
		<input class="btn-sticky" type="submit" value="登録">
	</form:form>
	</div>
</body>
</html>
