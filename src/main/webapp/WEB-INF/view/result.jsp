<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>テスト結果</title>
<link href="<c:url value="/resources/css/result.css" />" rel="stylesheet">
</head>
<body>
	<script type="text/javascript">
	history.pushState(null, null, null);
	$(window).on("popstate", function (event) {
	  if (!event.originalEvent.state) {
	    history.pushState(null, null, null);
	    return;
	  }
	});
	</script>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
	</div>
		<h1>　　　テスト結果</h1>
		<div id="contents">
		<c:if test="${points == 10}">
			<h2>合格</h2>
			<span>おめでとうございます</span>
		</c:if>
		<c:if test="${points != 10}">
			<h2>不合格</h2>
			<p>あなたの点数は <span>${points}</span> 点です</p>
		</c:if>
	</div>
	<br><h1>　　　解説</h1>
		<form:form modelAttribute="testForm" id="testForm" action="/eLearning/test/" method="post">
			<div class="explanation">
			<ul>
				<c:forEach items="${tests}" var="test">
				<p>
					<li class="question"><c:out value="${test.question}"></c:out></li>
					<div class="answers">
						①<c:out value="${test.answer1}"></c:out><br>
						②<c:out value="${test.answer2}"></c:out><br>
						③<c:out value="${test.answer3}"></c:out><br>
						④<c:out value="${test.answer4}"></c:out><br>
					</div>
					<div class="answer">
					答え&nbsp;<c:out value="${test.answer}"></c:out><br>
					</div>
					<div class="balloon"><p>
					解説<br>
					<c:out value="${test.explanation}"></c:out><br></p>
					</div>
				<p>
				</c:forEach>
			</ul>
				<div class="home">
					<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホームへ戻る</a>
				</div>
			</div>
		</form:form>
	</body>
</html>
