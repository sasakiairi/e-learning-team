<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード変更画面</title>
<link href="<c:url value="/resources/css/password.css" />" rel="stylesheet">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
</head>
<body>
	<div class="header">
		<div class="logo">
			<c:if test="${loginUser.loginCount != 0}">
				<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
			</c:if>
			<c:if test="${loginUser.loginCount == 0}">
				<img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100">
			</c:if>
		</div>
		<div class="link">
			<c:if test="${loginUser.loginCount != 0}">
				<c:if test="${loginUser.managementId == 1}">
					<a href="${pageContext.request.contextPath}/management/" class="btn-square-pop">ユーザー管理</a>
				</c:if>
				<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
			</c:if>
		</div>
	</div>
	<c:if test="${loginUser.loginCount == 0 || not empty errorMessages}">
	<div class="errorMessages">
		<c:if test="${loginUser.loginCount == 0}"><p>初期パスワードのままです<br>変更してください<p></c:if>
			<c:if test="${ not empty errorMessages }">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</c:if>
	</div>
	</c:if>
	<h1>　　　パスワード変更</h1>
	<div id="contents">
		<form:form modelAttribute="UserForm">
			新しいパスワード<br />
			<div class="cp_iptxt">
				<form:input type="password" class="field" path="password" pattern=".*\S+.*" placeholder="半角英数記号（6文字以上20文字以下）" required="required" /><br />
			</div>
			新しいパスワード(確認用)<br />
			<div class="cp_iptxt">
				<form:input type="password" class="field" path="confirmedPassword" pattern=".*\S+.*" placeholder="半角英数記号（6文字以上20文字以下）" required="required" /><br />
				<div class="checkbox"><input type="checkbox" id="password-check"><span>パスワードを表示する</span></div>
				<script type="text/javascript">
					const pwd = document.getElementById('password','confirmedPassword');
					const pwdCheck = document.getElementById('password-check');
					pwdCheck.addEventListener('change', function() {
						if (pwdCheck.checked) {
							pwd.setAttribute('type', 'text');
						} else {
							pwd.setAttribute('type', 'password');
						}
					}, false);
				</script>
			</div><br>
			<form:input type="hidden" path="id" value="${loginUser.id}"/>
			<form:input type="hidden" path="loginCount" value="1"/>
			<input class="btn-sticky" type="submit" value="変更"><br/>
		</form:form>
		<br/>
	</div>
</body>
</html>
