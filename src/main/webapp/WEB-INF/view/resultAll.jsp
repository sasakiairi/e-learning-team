<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>テスト結果一覧</title>
<link href="<c:url value="/resources/css/management.css" />"
	rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<br>
		<div class="link">
			<a href="${pageContext.request.contextPath}/management/"class="btn-square-pop">ユーザー管理</a>
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
	</div>
	<c:forEach items="${ testItems }" var="testItem" >
	<h1>　　　${testItem.testName} 受験状況一覧</h1>
	<div id="contents">
		<div id="userLists" class="clearfix"><br>
			合格者
			<c:forEach items="${users}" var="user">
				<ul>
					<li class="userlist"><c:if test="${user.managementId == 2}"><c:if test="${user.isPassed == 1}">
							<div class="name">
								<c:out value="${user.name}"></c:out>
							</div>
							<div class="user">
								<div class="account">
									アカウント：
									<c:out value="${user.account}"></c:out>
								</div>
								<div class="isPassed">
									<c:if test="${user.isPassed == 1}">受験状況：<span>合格</span></c:if>
								</div>
							</div>
						</c:if></c:if>
					</li>
				</ul>
			</c:forEach>
		</div>
		<br>
		<div id="userLists" class="clearfix">
			未合格者
			<c:forEach items="${users}" var="user">
				<ul>
					<li class="userlist"><c:if test="${user.managementId == 2}"><c:if test="${user.isPassed == 0}">
							<div class="name">
								<c:out value="${user.name}"></c:out>
							</div>
							<div class="user">
								<div class="account">
									アカウント：
									<c:out value="${user.account}"></c:out>
								</div>
								<div class="isNotPassed">
									<c:if test="${user.isPassed == 0}">受験状況：<span>未合格</span></c:if>
								</div>
							</div><br>
						</c:if></c:if>
					</li>
				</ul>
			</c:forEach>
		</div>
	</div>
	</c:forEach>
</body>
</html>
