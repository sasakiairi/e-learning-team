<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<c:if test="${loginUser.managementId == 1}">
				<a href="${pageContext.request.contextPath}/management/" class="btn-square-pop">ユーザー管理</a>
			</c:if>
			<a href="${pageContext.request.contextPath}/password/"  class="btn-square-pop">パスワード変更</a>
			<a href="${pageContext.request.contextPath}/logout/"  class="btn-square-pop">ログアウト</a>
		</div>
	</div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${ errorMessages }" var="errorMessage">
					<li><c:out value="${ errorMessage }" />
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" scope="session" />
		</div>
	</c:if>
	<h1>　　　試験一覧</h1>
		<div id="contents">
			<br>
			<c:forEach items="${ testItems }" var="testItem" >
			<a href="/eLearning/test/${testItem.testTable }" class="btn-sticky">${testItem.testName}</a>
			<c:if test="${loginUser.managementId == 1 }">&nbsp;&nbsp;&nbsp;
				 <a href="/eLearning/resultAll/${testItem.testTable}" class="btn-sticky2">受験状況一覧</a>
			</c:if>
			<c:if test="${loginUser.managementId == 2 }">&nbsp;&nbsp;&nbsp;
				<c:if test="${ loginUser.isPassed == 1 }" ><div class="btn-sticky2">合格</div></c:if>
			</c:if>
			</c:forEach>
			<br/><br/>
		</div>
	</body>
</html>
