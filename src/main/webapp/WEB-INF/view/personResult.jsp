<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${results.name}</title>
<link href="<c:url value="/resources/css/management.css" />" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<a href="${pageContext.request.contextPath}/management/" class="btn-square-pop">ユーザー管理</a>
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
	</div>
	<h1>　　　${results.name}の受験状況</h1>
		<div id="contents"><br>
			<c:forEach items="${ testItems }" var="testItem" >
				<div class="isNotPassed"><c:if test="${results.isPassed == 0}">${testItem.testName}：<span>未合格</span></c:if></div>
				<div class="isPassed"><c:if test="${results.isPassed == 1}">${testItem.testName}：<span>合格</span></c:if></div><br>
			</c:forEach>
		</div>
	</body>
</html>
