<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="<c:url value="/resources/css/management.css" />" rel="stylesheet">
</head>
<body>
	<script type="text/javascript">
		function message() {
			if (!window.confirm('削除しますか？')) {
				window.alert('キャンセルされました');
				return false;
			}
		}
	</script>

	<div class="header">
		<div class="logo">
			<a href="/eLearning/home/" class="home-link"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		</div>
		<div class="link">
			<a href="${pageContext.request.contextPath}/signup/" class="btn-square-pop">ユーザー新規登録</a>
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>
		</div>
	</div>
<h1>　　　登録社員一覧</h1>
	<div id="contents">
		<div id="userLists" class="clearfix"><br>
			管理者
			<c:forEach items="${users}" var="user">
			<ul>
			<li class="userlist">
				<c:if test="${user.managementId == 1}">
					<div class="name"><c:out value="${user.name}"></c:out></div>
					<div class="user">
						<div class="account">アカウント：<c:out value="${user.account}"></c:out></div>
						<div class="managementId"><c:if test="${user.managementId == 1}">役職：管理者</c:if></div>
						<c:if test="${user.id != loginUser.id}">
							<form:form modelAttribute="userForm" id="userForm" action="/eLearning/management/" method="post" onSubmit="return message()">
								<form:input type="hidden" path="id" value="${user.id}"/>
								<button class="delete" type="submit" >アカウント削除</button>
							</form:form>
						</c:if>
						<c:if test="${user.id == loginUser.id}">
							<button class="loginUser" >ログイン中</button>
						</c:if>
					</div>
					<p>
				</c:if>
			</li>
			</ul>
			</c:forEach>
		</div>
		<div id="userLists" class="clearfix">
			社員
			<c:forEach items="${users}" var="user">
			<ul>
			<li class="userlist">
				<c:if test="${user.managementId != 1}">
					<div class="name"><c:out value="${user.name}"></c:out></div>
					<div class="user">
						<div class="account">アカウント：<c:out value="${user.account}"></c:out></div>
						<div class="managementId"><c:if test="${user.managementId == 2}">役職：社員</c:if></div>
							<form:form modelAttribute="userForm" id="userForm" action="/eLearning/personResult/${user.id}" method="post" >
								<form:input type="hidden" path="id" value="${user.id}"/>
								<button class="delete" type="submit" >受験状況</button>
							</form:form>
							<form:form modelAttribute="userForm" id="userForm" action="/eLearning/management/" method="post" onSubmit="return message()">
								<form:input type="hidden" path="id" value="${user.id}"/>
								<button class="delete" type="submit" >アカウント削除</button>
							</form:form>
					</div>
					<p>
				</c:if>
			</li>
			</ul>
			</c:forEach>
		</div>
		<br>
	</div>
</body>
</html>
