<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
<link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100">
		</div>
	</div>
	<div class="main" id="contents">
		<h3>不正なパラメータが入力されました</h3>
		<h3>以下のどちらかを選択してください</h3>
		<div class="link">
			<a href="${pageContext.request.contextPath}/home/" class="btn-square-pop">ホーム</a>&nbsp;&nbsp;&nbsp;
			<a href="${pageContext.request.contextPath}/logout/" class="btn-square-pop">ログアウト</a>
		</div>
	</div>
</body>
</html>
