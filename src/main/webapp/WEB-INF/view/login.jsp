<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン画面</title>
<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="logo">
			<img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100">
		</div>
	</div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" scope="session" />
		</div>
	</c:if>
	<h1>　　　ログイン画面</h1>
	<div class="main" id="contents">
		<form:form modelAttribute="userForm">
			アカウント（社員番号）<br/>
			<div class="cp_iptxt">
				<form:input type="text" path="account" pattern=".*\S+.*" required="required" /><br/>
			</div>
			パスワード<br/>
			<div class="cp_iptxt">
				<form:input type="password" class="field" path="password" pattern=".*\S+.*" required="required" /><br/>
				<div class="checkbox"><input type="checkbox" id="password-check"><span>パスワードを表示する</span></div>
				<script type="text/javascript">
					const pwd = document.getElementById('password');
					const pwdCheck = document.getElementById('password-check');
					pwdCheck.addEventListener('change', function() {
						if (pwdCheck.checked) {
							pwd.setAttribute('type', 'text');
						} else {
							pwd.setAttribute('type', 'password');
						}
					}, false);
				</script>
			</div><br>
			<input class="btn-sticky" type="submit" value="ログイン"><br/>
		</form:form>
		<br/>
	</div>
</body>
</html>
