package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.entity.User;
import jp.co.kenshu.mapper.TestMapper;

@Service
public class ManagementService {

	@Autowired
	private TestMapper testMapper;

	public List<UserDto> getUserAll() {
		List<User> userList = testMapper.getUserAll();
		List<UserDto> resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserDto> convertToDto(List<User> userList) {
		List<UserDto> resultList = new LinkedList<>();
		for (User entity : userList) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public int deleteUser(int id) {
		int count = testMapper.deleteUser(id);
		return count;
	}

}
