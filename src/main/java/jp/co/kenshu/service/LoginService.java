package jp.co.kenshu.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.entity.User;
import jp.co.kenshu.mapper.TestMapper;

@Service
public class LoginService {

	@Autowired
	private TestMapper testMapper;

	public UserDto userSelect(String loginAccount) {
		User entity = testMapper.userSelect(loginAccount);
		if(entity == null) {
			return null;
		}
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
