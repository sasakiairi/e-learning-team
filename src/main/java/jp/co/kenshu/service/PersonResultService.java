package jp.co.kenshu.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.entity.User;
import jp.co.kenshu.mapper.TestMapper;

@Service
public class PersonResultService {
	@Autowired
	private TestMapper testMapper;
	public UserDto getResultByDto(UserDto dto) {
        User entity = testMapper.getResultByDto(dto);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

}
