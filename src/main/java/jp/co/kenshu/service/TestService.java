package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.AnswerIdDto;
import jp.co.kenshu.dto.TestDto;
import jp.co.kenshu.dto.TestListDto;
import jp.co.kenshu.entity.AnswerId;
import jp.co.kenshu.entity.Test;
import jp.co.kenshu.entity.TestList;
import jp.co.kenshu.mapper.TestMapper;

@Service
public class TestService {

	@Autowired
	private TestMapper testMapper;

	public List<TestDto> getTestAll(String testTable) {
		List<Test> testList = testMapper.getTestAll(testTable);
		List<TestDto> resultList = convertToDto(testList);
		return resultList;
	}

	private List<TestDto> convertToDto(List<Test> testList) {
		List<TestDto> resultList = new LinkedList<>();
		for (Test entity : testList) {
			TestDto dto = new TestDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public List<AnswerIdDto> getAnswerIdAll(String testTable) {
		List<AnswerId> answerIdList = testMapper.getAnswerIdAll(testTable);
		List<AnswerIdDto> resultList = convertToDto1(answerIdList);
		return resultList;
	}

	private List<AnswerIdDto> convertToDto1(List<AnswerId> answerIdList) {
		List<AnswerIdDto> resultList = new LinkedList<>();
		for (AnswerId entity : answerIdList) {
			AnswerIdDto dto = new AnswerIdDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public void changePass(int id, int passId) {
		testMapper.changePass(id, passId);
	}

	public List<TestListDto> getTestListAll() {
		List<TestList> testItem = testMapper.getTestListAll();
		List<TestListDto> resultList = convertToDto2(testItem);
		return resultList;
	}

	private List<TestListDto> convertToDto2(List<TestList> testItem) {
		List<TestListDto> resultList = new LinkedList<>();
		for (TestList entity : testItem) {
			TestListDto dto = new TestListDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}


}
