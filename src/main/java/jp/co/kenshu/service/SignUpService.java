package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.ManagementDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.entity.Management;
import jp.co.kenshu.entity.User;
import jp.co.kenshu.mapper.TestMapper;

@Service
public class SignUpService {

	@Autowired
	private TestMapper testMapper;

	public List<ManagementDto> getManagementAll() {
		List<Management> managementList = testMapper.getManagementAll();
		List<ManagementDto> resultList = convertToDto(managementList);
		return resultList;
	}

	private List<ManagementDto> convertToDto(List<Management> testList) {
		List<ManagementDto> resultList = new LinkedList<>();
		for (Management entity : testList) {
			ManagementDto dto = new ManagementDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public int insertUser(String account, String password, String name, int managementId) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encPassword = encoder.encode(password);
		int count = testMapper.insertUser(account, encPassword, name, managementId);
		return count;
	}

	public UserDto userSelect(String account) {
		User entity = testMapper.userSelect(account);
		if(entity == null) {
			return null;
		}
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}
}