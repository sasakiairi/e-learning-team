package jp.co.kenshu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jp.co.kenshu.mapper.TestMapper;

@Service
public class PasswordService {

	@Autowired
	private TestMapper testMapper;

	public int changePassword(int id, String password, int loginCount) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encPassword = encoder.encode(password);
		int count = testMapper.changePassword(id, encPassword, loginCount);
		return count;
	}
}
