package jp.co.kenshu.dto;

public class AnswerIdDto {
	private int answerId;

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
}
