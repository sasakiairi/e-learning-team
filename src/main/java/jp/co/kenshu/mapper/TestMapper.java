package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.entity.AnswerId;
import jp.co.kenshu.entity.Management;
import jp.co.kenshu.entity.Test;
import jp.co.kenshu.entity.TestList;
import jp.co.kenshu.entity.User;

public interface TestMapper {
	List<Test> getTestAll(@Param("testTable") String testTable);
	User userSelect(UserDto dto);
	List<Management> getManagementAll();
	List<User> getUserAll();
	int changePassword(@Param("id")int id, @Param("password")String encPassword, @Param("loginCount")int loginCount);
	int insertUser(@Param("account")String account,@Param("password")String encPassword,@Param("name")String name, @Param("managementId")int managementId);
	List<AnswerId> getAnswerIdAll(@Param("testTable") String testTable);
	User userSelect(String account);
	void changePass(@Param("id")int id, @Param("passId")int passId);
	int deleteUser(int id);
	List<TestList> getTestListAll();
	User getResultByDto(UserDto dto);
}
