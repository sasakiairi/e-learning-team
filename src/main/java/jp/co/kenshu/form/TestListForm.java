package jp.co.kenshu.form;

public class TestListForm {
	private Integer id;
	private String testTable;
	private String testName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTestTable() {
		return testTable;
	}
	public void setTestTable(String testTable) {
		this.testTable = testTable;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
}
