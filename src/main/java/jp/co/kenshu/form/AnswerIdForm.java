package jp.co.kenshu.form;

public class AnswerIdForm {

	private int answerId;

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
}
