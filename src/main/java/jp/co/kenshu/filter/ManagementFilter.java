package jp.co.kenshu.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.kenshu.dto.UserDto;

@WebFilter({"/management/" , "/signup/" , "/personResult/*" , "/resultAll/*"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		UserDto user = (UserDto) session.getAttribute("loginUser") ;
		String servletPath = req.getServletPath();
		int managementId = user.getManagementId();
		List<String> errorMessages = new ArrayList<String>();

		if ( managementId != 1 && !servletPath.contains(".css")) {
			errorMessages.add("アクセス権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("/eLearning/home/");
			return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}
