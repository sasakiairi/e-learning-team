package jp.co.kenshu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	private LoginService loginService;
	@Autowired
	HttpSession session;
	@RequestMapping(value = "/login/", method = RequestMethod.GET)
	public String login(Model model) {
		UserDto loginUser = null;
		session.setAttribute("loginUser", loginUser);
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		return "login";
	}
	@RequestMapping(value = "/login/", method = RequestMethod.POST)
	public String login (@ModelAttribute UserForm form, Model model) {
		String loginAccount = form.getAccount();
		String loginPassword = form.getPassword();
		UserDto loginUser = loginService.userSelect(loginAccount);
		List<String> errorMessages = new ArrayList<String>();
		if (loginUser == null) {
			errorMessages.add("ログインに失敗しました");
			model.addAttribute("errorMessages", errorMessages);
			model.addAttribute("loginAccount", loginAccount);
			model.addAttribute("loginPassword", loginPassword);
			return "login";
		}
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encPassword = loginUser.getPassword();
		if (!encoder.matches(loginPassword, encPassword)) {
			errorMessages.add("ログインに失敗しました");
			model.addAttribute("errorMessages", errorMessages);
			model.addAttribute("loginAccount", loginAccount);
			model.addAttribute("loginPassword", loginPassword);
			return "login";
		}
		session.setAttribute("loginUser", loginUser);
		if (loginUser.getLoginCount() == 0) {
			return "redirect:/password/";
		} else {
			return "redirect:/home/";
		}
	}
}
