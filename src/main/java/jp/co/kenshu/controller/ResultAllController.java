package jp.co.kenshu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.TestListDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.ManagementService;
import jp.co.kenshu.service.TestService;

@Controller
public class ResultAllController {
	@Autowired
	private ManagementService managementService;
	@Autowired
	private TestService testService;
	@RequestMapping(value = "/resultAll/{testTable}", method = RequestMethod.GET)
	public String UserAll(Model model,  @PathVariable String testTable) {
		List<UserDto> users = managementService.getUserAll();
		UserForm form = new UserForm();
		List<TestListDto> testItems = testService.getTestListAll();
		for(int i = 0; i < testItems.size(); i++) {
			if(!testItems.get(i).getTestTable().equals(testTable)){
				return "redirect:/error/";
			}
		}
		model.addAttribute("users", users);
		model.addAttribute("userForm", form);
		model.addAttribute("testItems", testItems);
		return "resultAll";
	}
}
