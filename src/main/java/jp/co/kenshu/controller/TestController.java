package jp.co.kenshu.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jp.co.kenshu.dto.AnswerIdDto;
import jp.co.kenshu.dto.TestDto;
import jp.co.kenshu.dto.TestListDto;
import jp.co.kenshu.form.AnswerIdForm;
import jp.co.kenshu.form.TestForm;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.TestService;

@Controller
public class TestController {
	@Autowired
	private TestService testService;
	@RequestMapping(value = "/test/{testTable}", method = RequestMethod.GET)
	public String TestAll(Model model, @PathVariable String testTable) {
		TestForm form = new TestForm();
		List<TestDto> tests = testService.getTestAll(testTable);
		List<TestListDto> testItems = testService.getTestListAll();
		for(int i = 0; i < testItems.size(); i++) {
			if(!testItems.get(i).getTestTable().equals(testTable)){
				return "redirect:/error/";
			}
		}
		model.addAttribute("testForm", form);
		model.addAttribute("tests", tests);
		model.addAttribute("testItems", testItems);
		return "testAll";
	}
	@RequestMapping(value = "/test/{testTable}", method = RequestMethod.POST)
	public String gettestInfo(@RequestParam("1") int answerNum1,
			@RequestParam("2") int answerNum2,
			@RequestParam("3") int answerNum3,
			@RequestParam("4") int answerNum4,
			@RequestParam("5") int answerNum5,
			@RequestParam("6") int answerNum6,
			@RequestParam("7") int answerNum7,
			@RequestParam("8") int answerNum8,
			@RequestParam("9") int answerNum9,
			@RequestParam("10") int answerNum10,
			@ModelAttribute AnswerIdForm form, UserForm userForm, Model model, @PathVariable String testTable) {
		List<TestDto> tests = testService.getTestAll(testTable);
		List<AnswerIdDto> answerIds = testService.getAnswerIdAll(testTable);
		List<Integer> answerNums = new ArrayList<Integer>();
		answerNums.add(answerNum1);
		answerNums.add(answerNum2);
		answerNums.add(answerNum3);
		answerNums.add(answerNum4);
		answerNums.add(answerNum5);
		answerNums.add(answerNum6);
		answerNums.add(answerNum7);
		answerNums.add(answerNum8);
		answerNums.add(answerNum9);
		answerNums.add(answerNum10);
		int points = 0;
		for(int i = 0; i < answerIds.size(); i++) {
			if(answerIds.get(i).getAnswerId() == answerNums.get(i)){
				points++;
			}
		}
		if (points == 10) {
			int passId = 1;
			testService.changePass(userForm.getId(), passId);
		}
		model.addAttribute("tests", tests);
		model.addAttribute("points", points);
		return "result";
	}
}
