package jp.co.kenshu.controller;import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.ManagementDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.SignUpService;
@Controller
public class SignUpController {
	@Autowired
	private SignUpService signUpService;
	@RequestMapping(value = "/signup/", method = RequestMethod.GET)
	public String userInsert(Model model) {
		List<ManagementDto> managements = signUpService.getManagementAll();
		UserForm form = new UserForm();
		model.addAttribute("managements" ,managements);
		model.addAttribute("UserForm", form);
		return "signup";
	}
	@RequestMapping(value = "/signup/", method = RequestMethod.POST)
	public String userInsert(@ModelAttribute UserForm form, Model model) {
		List<String> errorMessages = new ArrayList<String>();
		String password = form.getPassword();
		String confirmedPassword = form.getConfirmedPassword();
		String account = form.getAccount();
		String name = form.getName();
		UserDto differentUser = signUpService.userSelect(account);

		if (!isValid(differentUser, password, confirmedPassword,  account, name, errorMessages)) {

			List<ManagementDto> managements = signUpService.getManagementAll();
			model.addAttribute("errorMessages" ,errorMessages);
			model.addAttribute("managements" ,managements);
			model.addAttribute("UserForm", form);
			return "signup";
		}
		int count = signUpService.insertUser(form.getAccount(), form.getPassword(), form.getName(), form.getManagementId());
		Logger.getLogger(SignUpController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/management/";
	}
	private boolean isValid(UserDto differentUser, String password, String confirmedPassword, String account, String name, List<String> errorMessages) {
		if (differentUser != null) {
			errorMessages.add("ユーザーが重複しています");
		}
		if (account == null || account.isEmpty()) {
			errorMessages.add("アカウント名は7桁の数字(社員番号)で入力してください");
		}else if (!(account.length() == 7) || !(Pattern.matches("^[0-9]+$", account))) {
			errorMessages.add("アカウント名は7桁の数字(社員番号)で入力してください");
		}
		if (password == null || password.isEmpty()) {
			errorMessages.add("パスワードを入力してください");
		}else if (!(password.length() >= 6 && 20 >= password.length()) || !(Pattern.matches("^[-_@+*;:#$%&A-Za-z0-9]+$" , password))) {
			errorMessages.add("パスワードの形式が正しくありません");
		}
		if(!password.matches(confirmedPassword)) {
			errorMessages.add("パスワードと確認用パスワードが間違っています");
		}
		if (name == null || name.isEmpty()) {
			errorMessages.add("名前は20文字以内で入力してください");
		}else if (!(name.length() <= 20)) {
			errorMessages.add("名前は20文字以内で入力してください");
		}
		if(!(name.matches("^[a-zA-Z0-9０-９々ぁ-んァ-ン一-龥]+$"))){
			errorMessages.add("不適切な文字が使われています");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
