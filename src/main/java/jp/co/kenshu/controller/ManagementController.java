package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.ManagementDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.ManagementService;
import jp.co.kenshu.service.SignUpService;

@Controller
public class ManagementController {
	@Autowired
	private ManagementService managementService;
	@Autowired
	private SignUpService signUpService;
	@RequestMapping(value = "/management/", method = RequestMethod.GET)
	public String UserAll(Model model) {
		List<UserDto> users = managementService.getUserAll();
		List<ManagementDto> managements = signUpService.getManagementAll();
		UserForm form = new UserForm();
		model.addAttribute("users", users);
		model.addAttribute("managements" ,managements);
		model.addAttribute("userForm", form);
		return "management";
	}

	@RequestMapping(value = "/management/", method = RequestMethod.POST)
	public String deleteUser(@ModelAttribute UserForm form, Model model) {
		int count = managementService.deleteUser(form.getId());
		Logger.getLogger(ManagementController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:/management/";
	}
}
