package jp.co.kenshu.controller;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.TestListDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.TestForm;
import jp.co.kenshu.service.LoginService;
import jp.co.kenshu.service.TestService;

@Controller
public class HomeController {
	@Autowired
	private LoginService loginService;
	@Autowired
	private TestService testService;
	@Autowired
	HttpSession session;
	@RequestMapping(value = "/home/", method = RequestMethod.GET)
	public String TestAll(Model model) {
		UserDto user = (UserDto) session.getAttribute("loginUser");
		String loginAccount = user.getAccount();
		UserDto loginUser = loginService.userSelect(loginAccount);
		List<TestListDto> testItems = testService.getTestListAll();
		TestForm form = new TestForm();
		model.addAttribute("testForm", form);
		model.addAttribute("testItems", testItems);
		session.setAttribute("loginUser", loginUser);
		return "home";
	}
}
