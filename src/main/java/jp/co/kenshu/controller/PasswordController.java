package jp.co.kenshu.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.PasswordService;

@Controller
public class PasswordController {
	@Autowired
	private PasswordService passwordService;
	@RequestMapping(value = "/password/", method = RequestMethod.GET)
	public String changePassword( Model model) {
		UserForm form = new UserForm();
		model.addAttribute("UserForm", form);
		return "password";
	}
	@RequestMapping(value = "/password/", method = RequestMethod.POST)
	public String changePassword(@ModelAttribute UserForm form, Model model) {
		List<String> errorMessages = new ArrayList<String>();
		String password = form.getPassword();
		String confirmedPassword = form.getConfirmedPassword();
		if (!isValid(password, confirmedPassword, errorMessages)) {
			model.addAttribute("errorMessages" ,errorMessages);
			model.addAttribute("UserForm", form);
			return "password";
		}
		int count = passwordService.changePassword(form.getId(), form.getPassword(), form.getLoginCount());
		Logger.getLogger(PasswordController.class).log(Level.INFO, "変更件数は" + count + "件です。");
		return "redirect:/home/";
	}
	private boolean isValid(String password, String confirmedPassword, List<String> errorMessages) {
		if (password == null || password.isEmpty()) {
			errorMessages.add("パスワードを入力してください");
		}else if (!(password.length() >= 6 && 20 >= password.length()) || !(Pattern.matches("^[-_@+*;:#$%&A-Za-z0-9]+$" , password))) {
			errorMessages.add("パスワードの形式が正しくありません");
		}
		if(!password.matches(confirmedPassword)) {
			errorMessages.add("パスワードと確認用パスワードが間違っています");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
