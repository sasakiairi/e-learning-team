package jp.co.kenshu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.TestListDto;
import jp.co.kenshu.dto.UserDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.PersonResultService;
import jp.co.kenshu.service.TestService;

@Controller
public class PersonResultController {
	@Autowired
	private PersonResultService personResultService;
	@Autowired
	private TestService testService;
	@RequestMapping(value = "/personResult/{id}", method = RequestMethod.GET)
	public String PersonResult( Model model, @PathVariable int id) {
		UserDto dto = new UserDto();
		dto.setId(id);
		UserDto results = personResultService.getResultByDto(dto);
		List<TestListDto> testItems = testService.getTestListAll();
		model.addAttribute("results", results);
		model.addAttribute("testItems", testItems);
		return "personResult";
	}

	@RequestMapping(value = "/personResult/{id}", method = RequestMethod.POST)
	public String PersonResult(@ModelAttribute UserForm form, Model model) {
		int id = form.getId();
		return "redirect:/personResult/" + id;
	}
}
