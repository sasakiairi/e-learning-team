<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<title>ユーザー管理</title>
<link href="<c:url value="/resources/css/management.css" />" rel="stylesheet">
</head>
<body>
<div class="header">
</div>
	<h1>ユーザー管理</h1>

	<div class="users">
		<c:forEach items="${users}" var="user">
		<div class="name"><c:out value="${user.name}"></c:out></div>
			<div class="user"><p>
				<div class="account">アカウント：<c:out value="${user.account}"></c:out></div>
				<div class="password">パスワード：<c:out value="${user.password}"></c:out></div>
				<div class="managemantId">管理者権限：<c:out value="${user.managementId}"></c:out></div>
				<div class="isPassed">受験状況：<c:out value="${user.isPassed}"></c:out></div><p>
			</div>
			<p>
		</c:forEach>
	</div>
</body>
</html>