<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<title>ログイン画面</title>
</head>
<body>

	<div class="header"></div>
	<h1>ログイン画面</h1>

	<form:form modelAttribute="userForm">
		アカウント（社員番号）<br />
		<form:input path="account" />
		<br />
		パスワード<br />
		<form:input path="password" />
		<br />
		<input type="submit">
	</form:form>

</body>
</html>