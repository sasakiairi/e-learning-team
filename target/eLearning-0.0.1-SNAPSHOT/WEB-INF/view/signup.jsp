<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<title>新規登録</title>
</head>
<body>
	<a href="http://localhost:8080/eLearning/management/" class="url">ユーザー一覧</a>

	<h1>ユーザー新規登録</h1>
	<form:form modelAttribute="UserForm">
        アカウント名：<form:input path="account" />
		<br>
        パスワード：<form:input path="password" />
		<br>
        名前：<form:input path="name" />
		<br>
		<p>
			役職： <select name="managementId" id="managementId">
				<c:forEach items="${managements}" var="management">
					<option value="${management.id}"><c:out
							value="${management.name}" /></option>
				</c:forEach>
			</select>
		</p>
		<input type="submit" value="登録">
	</form:form>
</body>
</html>